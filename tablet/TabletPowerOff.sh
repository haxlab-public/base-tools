#!/bin/bash
# TabletOn.py - turn on a tablet via canakit USB realy board.
#

# "Press" tablet power button for 8 seconds via relay 3
python ../relay/relay.py 3 -ts 8
