#!/bin/bash
# TabletOn.py - turn on a tablet via canakit USB realy board.
#

# "Press" tablet power button for 8 seconds via relay 3
source /home/researcher1/.bashrc
pwd
cd "$(dirname "$0")";
pwd
python ../relay/relay.py 3 -ts 8
