#!/bin/bash
# TabletOn.py - turn on a tablet via canakit USB realy board.
#

# To wake up tablet, "press" the power button (via relay 3) quickly several times.
python ../relay/relay.py 3 -ts 0.5
sleep 0.5
python ../relay/relay.py 3 -ts 0.5
sleep 0.5

# "Press" power button 3 seconds to power on.
python ../relay/relay.py 3 -ts 3
