#!/bin/bash
# TabletOn.sh - turn on a tablet via canakit USB realy board.
#


wait_file() {
	local file="$1"; shift
	local wait_seconds="${1:-10}"; shift # 10 sec default
	until test $((wait_seconds--)) -eq 0 -o -f "$file"; do sleep 1; done

	((++wait_seconds))
}

# To wake up tablet, "press" the power button (via relay 3) quickly several times.
source ~/.bashrc
cd "$(dirname "$0")";
wait_file "/dev/canakit" 15
python ../relay/relay.py 3 -ts 0.5
sleep 0.5
python ../relay/relay.py 3 -ts 0.5
sleep 0.5

# "Press" power button 3 seconds to power on.
python ../relay/relay.py 3 -ts 3
