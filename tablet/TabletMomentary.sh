#!/bin/bash
# Tablet.py - turn on a tablet via canakit USB relay board.
#

# "Press" tablet power button for 3 seconds via relay 3
python ../relay/relay.py 3 -ts 3
