#
# relay.py - toggle a relay on a USB cannakit relay board.
#

import serial
import time
import argparse
import os


def checkIfCanakitIsPresent():
    if os.path.exists('/dev/canakit'):
        return True
    else:
        return False

# Get command line arguments
parser = argparse.ArgumentParser(description='Toggle relays on a Cannakit relay board.')
parser.add_argument('relayNumber', type=int, default=1, help='Relay to toggle')

# Only allow one relay command
cmdGroup = parser.add_mutually_exclusive_group(required=True)
cmdGroup.add_argument('-s','--set', dest='setCmd', action='store_true', help='Set the indicated relay on.')
cmdGroup.add_argument('-c','--clear', dest='clearCmd', action='store_true', help='Set the indicated relay off.')
cmdGroup.add_argument('-ts','--toggleSet', action='store', type=float, dest='toggleSetTimeSeconds', metavar='seconds', help='Toggle the indicated relay on for a specified time.')
cmdGroup.add_argument('-tc','--toggleClear', action='store', type=float, dest='toggleClearTimeSeconds', metavar='seconds', help='Toggle the indicated relay off for a specified time.')

args = parser.parse_args()

# Wrapper to encode strings as bytes before writing.
def encodeWrite(ser, s):
    ser.write(s.encode())

# Are we toggling relay on?

if checkIfCanakitIsPresent() == True:

    if args.toggleSetTimeSeconds is not None:
        print('Toggling relay %d on for %0.1f seconds.' %(args.relayNumber, args.toggleSetTimeSeconds))
        ser = serial.Serial('/dev/canakit',115200, timeout=1)
        encodeWrite(ser,'\n\r')
        val = ser.read(size=64)
        print (val.decode("utf-8"))
        encodeWrite(ser,'REL%d.ON\n\r' % args.relayNumber)
        time.sleep(args.toggleSetTimeSeconds)
        encodeWrite(ser,'REL%d.OFF\n\r' % args.relayNumber)
        val = ser.read(size=64)
        print (val.decode("utf-8"))
        ser.close()
    # Toggling relay off?
    elif args.toggleClearTimeSeconds is not None:
	    print('Toggling relay %d off for %0.1f seconds.' %(args.relayNumber, args.toggleClearTimeSeconds))
	    ser = serial.Serial('/dev/canakit',115200, timeout=1)
	    encodeWrite(ser,'\n\r')
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    encodeWrite(ser,'REL%d.ON\n\r' % args.relayNumber)
	    time.sleep(args.toggleClearTimeSeconds)
	    encodeWrite(ser,'REL%d.OFF\n\r' % args.relayNumber)
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    ser.close()
    # Turning relay on?
    elif args.setCmd:
	    print('Turning on relay %d.' %(args.relayNumber))
	    ser = serial.Serial('/dev/canakit',115200, timeout=1)
	    encodeWrite(ser,'\n\r')
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    encodeWrite(ser,'REL%d.ON\n\r' % args.relayNumber)
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    ser.close()
    # Turning relay off?
    elif args.clearCmd:
	    print('Turning off relay %d.' %(args.relayNumber))
	    ser = serial.Serial('/dev/canakit',115200, timeout=1)
	    encodeWrite(ser,'\n\r')
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    encodeWrite(ser,'REL%d.OFF\n\r' % args.relayNumber)
	    val = ser.read(size=64)
	    print (val.decode("utf-8"))
	    ser.close()
    else:
	    print('ERROR: Must select soemthing for the script to do (set, celar, or toggle).')
	    args.usage()
else:
    print("ERROR: The canakit relay board is not present")
    exit(-1)


